import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Alert, Button  } from 'react-native'
import { NativeBaseProvider, Select, CheckIcon} from 'native-base';
import I18n from './src/localize/i18n';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RNLocalize from "react-native-localize";

const App = () => {
  // const d = RNLocalize.getLocales();
  // console.log(RNLocalize.getLocales());

  // const local = d[0].languageCode;
  const[Lang, setLang] = useState('')
  
  // useEffect(() => {
  //   I18n.locale = local
  // })

  useEffect(() => {
     // AsyncStorage.removeItem('lang')
      getData()
   
    // if(val){
    //   I18n.locale = val
    //   setLang('hhhh')
    // }
   
  
  })

  const getData = async () => {
    try {
      const val = await AsyncStorage.getItem('lang')
      console.log("NAME: ",val);
      if(val){
        I18n.locale = val
        setLang('hhhh')
      }
      
    } catch (error) {
      console.log(error);
    }
    
  }
  

  const onClick = async (param) => {
    await AsyncStorage.setItem(
      'lang', param
    )
    setLang('fds')

    
  }
  
  return (
    <NativeBaseProvider>
      <View style={[styles.container]}>
      <Button title="Click" 
        onPress={()=> onClick()}
      />
      <Text style={styles.language}>
        Change Language
      </Text>
      <Text style={styles.language}>{I18n.t('name')}</Text>
      <Select
        fontSize={20}
        selectedValue={Lang}
        minWidth="300"
        textAlign="center"
        accessibilityLabel="Change language"
        placeholder="Change language"
        _selectedItem={{
          bg: "teal.200",
          endIcon: <CheckIcon size="5" />,
        }}
        mt={1}
        onValueChange={(itemValue) => setLang(itemValue)}
      >
        <Select.Item label="English" value="en"
          onPress= {() => {
                  // const data = I18n.locale = 'en';
                  onClick('en')
                 setLang("en");
                 
                }}
        />
        <Select.Item label="Khmer" value="km" 
          onPress= {() => {
                  //const data = I18n.locale = 'km';
                  onClick('km')
                 setLang("km");
                }}
        />
      </Select>
      </View>
    </NativeBaseProvider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  language: {
    fontSize: 35,
  },
})
export default App
