import I18n from 'react-native-i18n';
import * as RNLocalize from "react-native-localize";

import km from './km';
import en from './en';

const d = RNLocalize.getLocales();

I18n.fallbacks = true;
I18n.locale = 'en';


I18n.translations = {
  km,
  en,
};
export default I18n;
